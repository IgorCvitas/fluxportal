# API Gateway


> An API gateway is an API management tool that sits between a client and a collection of backend services. An API gateway acts as a reverse proxy to accept all application programming interface (API) calls, aggregate the various services required to fulfill them, and return the appropriate result.

## Overview
API Gateway service is a simple (no function included) serverless framework project which serves as an AWS CloudFormation stack creation tool to unify all Flux services and their endpoints. Although such service can be easily created through CloudFormation or other IaaS providers, for the simplicity of implementation serverless framework has been chosen to use for creating this service. 

## Infrastructure
- AWS API Gateway
- Cognito User Pool Authorizer
- Cloudwatch Logs
- X-Ray tracing

## Integrations
In order to include services within this API Gateway, it is simply needed to reference the API gateway resources under the `provider` section within the serverless.yml file. Once the service is deployed, API Gateway will update the stack and add service endpoints.

```yaml
provider:
  apiGateway:
    restApiId:
      'Fn::ImportValue': ${self:provider.stage}-FluxGateway-restApiId
    restApiRootResourceId:
      'Fn::ImportValue': ${self:provider.stage}-FluxGateway-rootResourceId
```

Cognito user pool authorizer available in the API Gateway is provided by Cognito User Pool defined User API service. Authorizer can be shared and integrated within others services by importing the authorizer ID under the function's `authorizer` settings:
```yaml
functions:
  function_name:
    handler: src/handlers.handler
    events:
      - http:
          path: path/of/endpoint
          method: get
          authorizer:
            type: COGNITO_USER_POOLS
            authorizerId: 
              'Fn::ImportValue': FluxGateway-authorizerId
```


## Roadmap
- Better observability through DataDog or similar monitoring solutions

## Research 
- Migration to deployment of API Gateway through IaaS provider such as Terraform. Currently only seen worth in case of switching from Serverless Framework to much more lightweight solutions such as arc.codes or Chalice.

## API Documentation
API documentation for API Gateway is defined in services which integrate to it.
