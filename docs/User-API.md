# User-API

## Overview
User API service provides the core `user` functionality to the Flux system, allowing creation of user accounts and profile management. 

<!-- theme: info -->
>Authentication and authorization are supported by AWS Cognito.
After a successful authentication, Amazon Cognito returns user pool tokens  - `JWT`. Tokens can be used to grant users access to erver-side resources or to the Amazon API Gateway endpoints.

Amazon Cognito user pools implements `ID`, `access`, and `refresh` tokens as defined by the OpenID Connect (OIDC) open standard:
- The ID token contains claims about the identity of the authenticated user such as name, email, and phone_number.
- The access token contains scopes and groups and is used to grant access to authorized resources.
- The refresh token contains the information necessary to obtain a new ID or access token.


### Currently supported features:
- User roles: Shopper & Admin
- Create account
- Account confirmation with email verification
- Login / logout (create / refresh JWT)
- Password recovery
- Profile update
- GET profile endpoint


## Architecture
![RexAPI](../assets/images/UserAPI.png)

## Infrastructure
- Cognito User Pools provider authentication and authorization
- Cognito Authorization is integrated with API Gateway
- User roles are defined as Cognito Pool Groups
- Cognito Identity Providers provide IAM roles per group (or user)
- Profile data is stored in DynamoDB
- Profile media is stored in S3 bucket
- AWS CloudFormation stack for creating owned infrastructure & deployment
- AWS Lambda for serverless computing
- API Gateway (integration) serving endpoints
- IAM permissions for granting access to other services
- SSM Parameter Store for sharing dynamic environment variables
- CloudWatch for logging
- AWS X-Ray for tracing


## Roadmap
- User authentication with: Facebook, Google & Apple Id
- Better access permissions based on IAM Roles
- User roles and profiles: Moderator, Influencer, Retailer
- Delivery of service related e-mail through AWS SES (Simple Email Service)
- User settings management

## Research 
- Issues with with token invalidations: https://github.com/aws-amplify/amplify-js/issues/3435


