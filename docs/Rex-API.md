# Rex-API

## Overview

Rex API is a simple service that allows integration of Retail Explorer (REX) with the 
Flux system. 

** Features: **
1. Save REX configuration files
2. Return REX configuration files
3. Save REX configuration test files
4. Return REX configuration test files

Service provides the functionality of getting and saving retailer configuration files for parsing the product data & configuration file tests used for testing the selectors in configuration files. 

<!-- theme: info -->
> With the help of Rex API’s custom integration with Product Finder, Retailer Explorer is able to send a request which returns all available selectors and parsed results for the given product URL. (OUTDATED: sync invocation, working but better solution provided)



```yaml http

{
  "method": "get",
  "url": "https://pmnl9nthr9.execute-api.eu-central-1.amazonaws.com/dev-igor/rex/product_selectors",
  "query": {
    "url": ""
  },
  "headers": {
    "Content-Type": "application/json"
  }
}
```

## Architecture
![RexAPI](../assets/images/ProductAPI.png)

## Workflow diagrams

## Infrastructure
- AWS CloudFormation stack for creating owned infrastructure & deployment
- AWS Lambda for serverless computing
- API Gateway (integration) serving endpoints
- AWS Cognito authorization
- S3 bucket for storing configuration files and tests
- IAM permissions for granting access to other services
- SSM Parameter Store for sharing dynamic environment variables
- CloudWatch for logging
- AWS X-Ray for tracing

## Integrations
All integrations to Rex API service can be made through available REST endpoints. See API documentation for more details.

## Work in progress 
- The authorization layer has been successfully implemented & waiting commented for Retailer Explorer to support the functionality.

## Roadmap
- Event based Integration with Retailer Finder on the successful upload of new configuration files.
- The transition from storing the configurations JSON files in S3 Bucket to DynamoDB database
- Automated testing of configuration files

