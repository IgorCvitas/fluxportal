# Retailer-Finder

## Overview
Retailer Finder is a simplifed version of a Product Finder service. Based on Parsel library, it is using only hardcoded XPath selectors to parse common metadata from a webpage. 


## Architecture
![RexAPI](../assets/images/RetailerParser.png)


## Infrastructure
- AWS CloudFormation stack for creating owned infrastructure & deployment
- AWS Lambda for serverless computing
- API Gateway (integration) serving endpoints
- IAM permissions for granting access to other services
- SSM Parameter Store for sharing dynamic environment variables
- CloudWatch for logging
- AWS X-Ray for tracing
- Crawlera Proxy


## Roadmap
- Trigger each time when a new retailer configuration file has been saved
- Trigger each time a product from a new retailer has been reqested (triggered before Product Finder so that `product data` can be fully saved)
- Improve fetching of retailer logo
- Post processing (data cleaning) functions


