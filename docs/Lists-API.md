# Lists-API

The beginning of an awesome service...


## As a user…

    I want a default list (E1)
    I want to create a list (E1)
	I want to view the list (E1)
	I want to edit the list details (E1)
	I want to delete the list (E1)
	I want to add items to the list (E1)
	I want to mark items are purchased (Update)(Epic N)
	I want to remove items from the list (E1)
	I want to see a list of all of my lists (E1)
	I want to clone existing list (EN)
	I want to share my list with other users (EN)
	I want to share list with publicly (EN)
	I want to invite others users to collaborate on my lists (EN)
    I want to sort products in the list (EN)
    I want to order products in the list (by my choice) (EN)
    I want to follow the public list (EN)
    I want to add product quantity (EN)


## As a list…
	I want an owner (E1)
	I want only distinct items added to the list (E1)
	I want a profile attributes (name, slug, description,..) (E1)
	I want either multiple owners or collaborators (EN)
	I want to display _______ data (product variations, retailer, brand,…) (E1+)

## As a Flux…
	I want to save list related data to standalone database
	I want to user DynamoDB for storing list data
	I want to identify list owners & products by their ID
	I want the List API to serve only as reference to products
	I want List API to be a standalone service
	I want to allow access to list details & products to list owner
	I want to allow access to list details & products to collaborators
	I want to allow access of all lists, their details & products to admin
	I want to store all lists media to S3 bucket
	I want to store all secret environment files to SSM Parameter Store



 

# Epic 1: Basic List API functionality

Description: Create basic CRUD functionlity of List API service

## (C)reate
- I want a default list
- I want to create a list
- I want to add items to the list

## (R)read
- I want to see all of my lists
- I want to view all the products from the list

## (U)pdate
- I want to edit the list details

## (D)delete
- I want to delete the list
- I want to remove items from the list

* * *
TODO: Add function specific user stories

* * *
**User Story (feature): ** As a **_user_**, I want a **_default list created_** when I **_create an account_** on Basket.

`CRUD info:` Create a default list

`Persona:` User

`GIVEN:` User account details

`WHEN:` User is creating and account

`THEN:` Create a default list for new user
* * *

**User Story (feature): ** As a **_user_**, I want to **_create a new list_** to which a I can assign a **_custom name _** & **_short description_**.

`CRUD info:` Create a new list

`Persona:` User

`GIVEN:` Name & short description of a new list

`WHEN:` Creating a new list

`THEN:` Create a new list
* * *

**User Story (feature): ** As a **_user_**, I want to be able **_add products_** to the **_lists_** (I own)

`CRUD info:` Add products to the list

`Persona:` User

`GIVEN:` Product

`WHEN:` Adding product to the list

`THEN:` Save product ID to the list
* * *

**User Story (feature): ** As a **_user_**, I want an **_overview_** of all of my **_lists_**

`CRUD info:` Read all lists

`Persona:` User

`GIVEN:` User ID

`WHEN:` Request to overview lists

`THEN:` View all lists
* * *

**User Story (feature): ** As a **_user_**, I want to **_view_** all the **_products_** from the **_list_**.

`CRUD info:` View products in the list

`Persona:` User

`GIVEN:` List ID

`WHEN:` Viewing list

`THEN:` View all products from the list
* * *

**User Story (feature): ** As a **_user_**, I want to **_edit_** the list **_name_** and **_description_**

`CRUD info:` Update list details

`Persona:` User

`GIVEN:` List ID, Updated name and/or description

`WHEN:` Updating list details

`THEN:` Updated list details
* * *

**User Story (feature): ** As a **_user_**, I want to **_delete a list_** (which I have created)

`CRUD info:` Delete list

`Persona:` User

`GIVEN:` List ID

`WHEN:` Requesting to delete list

`THEN:` Delete the list and all related resources (list details)
* * *

**User Story (feature): ** As a **_user_**, I want to **_delete items_** from the list

`CRUD info:` Delete products from the list

`Persona:` User

`GIVEN:` Product ID, List ID

`WHEN:` Deleting item from the list

`THEN:` Delete item from the list
* * *

