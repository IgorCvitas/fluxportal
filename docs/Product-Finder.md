# Product-Finder

## Overview
Product Finder service is a serverless `headless` parsing tool inspired by ScrapRT. Built using Scrapy's parsing library `Parsel`, Product Finder accepts url parameter through API endpoint, downloads the requested page, parses the data using default selectors and/or external configuration and return the parsed content. 

## Anti Robot protections
- Downloads the content through requests session with rotating fake user agents
- Integrated Crawlera Proxy

## External Configuration files
Product Finder checks for external configuration files based on the requested URL. In case the configuration files exists, loads the selectors from configuration file and parses the content. In case of missing external configuration file, Product Finder will parse the content using the default XPath selector

**Configuration model:**

```
{
	"retailer_name": "Retailer-test",
	"retailer_url": "www.url-of-a-retailer.com",
	"selectors": {
		"title": {
			"xpath": ["xpath1", "xpath2"],
			"css": ["css1", "css2"]
		},
		"description": {
			"xpath": ["xpath1", "xpath2"],
			"css": ["css1", "css2"]
		},
		"image": {
			"xpath": ["xpath1", "xpath2"],
			"css": ["css1", "css2"]
		},
		"currency": {
			"xpath": ["xpath1", "xpath2"],
			"css": ["css1", "css2"]
		},
		"brand": {
			"xpath": ["xpath1", "xpath2"],
			"css": ["css1", "css2"]
		},
		"locale": {
			"xpath": ["xpath1", "xpath2"],
			"css": ["css1", "css2"]
		},
		"json": {
            "xpath" : ["xpath1"]
        }
	},
	"functions": {
		"title": "function-name",
		"description": "function-name",
		"image": "function-name",
		"price": "function-name",
		"currency": "function-name",
		"brand": "function-name",
		"locale": "function-name"
	}
}

```

## Integration with REX
If the Product Finder receives the parsing request with included `source_origin=rex` parameter, it will return all possible (non-empty) values & selectors used for parsing, to help REX create better external configuration. 

Returned response can be saved by Rex API as test for testing purposes. 

## Architecture
![RexAPI](../assets/images/ProductParser.png)


## Infrastructure
- AWS CloudFormation stack for creating owned infrastructure & deployment
- AWS Lambda for serverless computing
- API Gateway (integration) serving endpoints
- IAM permissions for granting access to other services
- SSM Parameter Store for sharing dynamic environment variables
- CloudWatch for logging
- AWS X-Ray for tracing
- Crawlera Proxy Integrations
- S3 bucket for storing configuration files & tests


## Roadmap
- Refactor workflow (event driven split)
- Improve external configuration 
- Store external configuration in DynamoDB
- More Post processing functions
- XPath selectors for @schema.org 
- Support CSS selectors
- Scheduled configuration tests


## Research 
- Headless browser integration
- New configuration structure to include function per selector, note, source, custom attribute,..


** Default Selectors: **
```
META_SELECTORS = {
    'title': [
        '/html/head/meta[@property="og:title"]/@content',
        '/html/head/meta[@property="twitter:title"]/@content',
        '/html/head/meta[@property="sailthru.title"]/@content',
        '/html/head/meta[@property="title"]/@content',
        '/html/head/meta[@itemprop="name"]/@content',
        '/html/head/meta[@name="og:title"]/@content',
        '/html/head/meta[@name="twitter:title"]/@content',
        '/html/head/meta[@name="sailthru.title"]/@content',
        '/html/head/meta[@name="twitter:title"]/@content'
        ],
    'description': [
        '/html/head/meta[@name="og:description"]/@content',
        '/html/head/meta[@name="description"]/@content',
        '/html/head/meta[@name="twitter:description"]/@content',
        '/html/head/meta[@property="og:description"]/@content',
        '/html/head/meta[@property="description"]/@content',
        '/html/head/meta[@property="twitter:description"]/@content'
        ],
    'price': [
        '/html/head/meta[@name="og:price"]/@content',
        '/html/head/meta[@name="og:price:amount"]/@content',
        '/html/head/meta[@name="product:price:amount"]/@content',
        '/html/head/meta[@property="og:price"]/@content',
        '/html/head/meta[@property="og:price:amount"]/@content',
        '/html/head/meta[@property="product:price:amount"]/@content'
        ],
    'currency': [
        '/html/head/meta[@name="og:currency"]/@content',
        '/html/head/meta[@name="product:price:currency"]/@content',
        '/html/head/meta[@name="og:price:currency"]/@content',
        '/html/head/meta[@property="og:currency"]/@content',
        '/html/head/meta[@property="og:price:currency"]/@content',
        '/html/head/meta[@property="product:price:currency"]/@content'
        ],
    'image': [
        '/html/head/meta[@name="og:image"]/@content',
        '/html/head/meta[@name="og:image:url"]/@content',
        '/html/head/meta[@property="og:image"]/@content',
        '/html/head/meta[@property="og:image:url"]/@content'
        ],
    'locale': [
        '/html/head/meta[@name="og:locale"]/@content',
        '/html/@lang'
        ]
    }
```
