# Product-API

## Overview
Product API is a `core` service of Flux System. 
It is in charge of aggregating, tracking and delivering product data to services and consumers. 
<!-- theme: info -->
> Key challenge of the Product API is to maintain correct, relevant and up to date information about the products.

<!-- theme: danger -->
> ### WARNING! 
Failure of maintaining the data integrity within the Product API is a critical point of failure of an entire system! 


## Data model
```md
Product data model has been defined around the concept in which:
  - Every `product` has a `variation`.
  - Every `product` has `options`.
  - Every `option` has `values`.
  - Every `variation` represents a combination of `options` and their `values`.
```

** Features supported by current model: **
 - Identification by global product identifiers (GTIN, UPC, EAN, MPN, ISBN)
 - Storing multiple product media (image, video & audio)
 - Time based product data chages
 - Multilingual data
 - Storing & mapping product variations
 - Mutiple retailer support
 - Product offer history (time based tracking of price and availability)




** Data model consists of following collections: **
<!-- theme: success -->
> ### product_group
Collection of all products (variations)

<!--
type: tab
title: Schema
-->

```json json_schema
{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "required": [],
    "properties": {
        "_id": {
            "$id": "#/properties/_id"
        },
        "group_id": {
            "$id": "#/properties/group_id"
        },
        "created_at": {
            "$id": "#/properties/created_at"
        },
        "updated_at": {
            "$id": "#/properties/updated_at"
        },
        "media": {
            "$id": "#/properties/media",
            "required": [],
            "properties": {
                "images": {
                    "$id": "#/properties/media/properties/images",
                    "items": {
                        "$id": "#/properties/media/properties/images/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/media/properties/images/items/anyOf/0",
                                "required": [],
                                "properties": {
                                    "source_url": {
                                        "$id": "#/properties/media/properties/images/items/anyOf/0/properties/source_url"
                                    },
                                    "source": {
                                        "$id": "#/properties/media/properties/images/items/anyOf/0/properties/source"
                                    },
                                    "key": {
                                        "$id": "#/properties/media/properties/images/items/anyOf/0/properties/key"
                                    }
                                }
                            }
                        ]
                    }
                },
                "videos": {
                    "$id": "#/properties/media/properties/videos",
                    "items": {
                        "$id": "#/properties/media/properties/videos/items"
                    }
                },
                "audios": {
                    "$id": "#/properties/media/properties/audios",
                    "items": {
                        "$id": "#/properties/media/properties/audios/items"
                    }
                }
            }
        },
        "global_identifiers": {
            "$id": "#/properties/global_identifiers",
            "required": [],
            "properties": {
                "gtin": {
                    "$id": "#/properties/global_identifiers/properties/gtin"
                },
                "isbn": {
                    "$id": "#/properties/global_identifiers/properties/isbn"
                },
                "jan": {
                    "$id": "#/properties/global_identifiers/properties/jan"
                },
                "upc": {
                    "$id": "#/properties/global_identifiers/properties/upc"
                },
                "ean": {
                    "$id": "#/properties/global_identifiers/properties/ean"
                },
                "mpn": {
                    "$id": "#/properties/global_identifiers/properties/mpn"
                }
            }
        },
        "details": {
            "$id": "#/properties/details",
            "required": [],
            "properties": {
                "name": {
                    "$id": "#/properties/details/properties/name",
                    "items": {
                        "$id": "#/properties/details/properties/name/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/details/properties/name/items/anyOf/0",
                                "required": [],
                                "properties": {
                                    "value": {
                                        "$id": "#/properties/details/properties/name/items/anyOf/0/properties/value"
                                    },
                                    "retailer_id": {
                                        "$id": "#/properties/details/properties/name/items/anyOf/0/properties/retailer_id"
                                    },
                                    "locale": {
                                        "$id": "#/properties/details/properties/name/items/anyOf/0/properties/locale"
                                    }
                                }
                            }
                        ]
                    }
                },
                "description": {
                    "$id": "#/properties/details/properties/description",
                    "items": {
                        "$id": "#/properties/details/properties/description/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/details/properties/description/items/anyOf/0",
                                "required": [],
                                "properties": {
                                    "value": {
                                        "$id": "#/properties/details/properties/description/items/anyOf/0/properties/value"
                                    },
                                    "retailer_id": {
                                        "$id": "#/properties/details/properties/description/items/anyOf/0/properties/retailer_id"
                                    },
                                    "locale": {
                                        "$id": "#/properties/details/properties/description/items/anyOf/0/properties/locale"
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        },
        "offers": {
            "$id": "#/properties/offers",
            "items": {
                "$id": "#/properties/offers/items",
                "anyOf": [
                    {
                        "$id": "#/properties/offers/items/anyOf/0",
                        "required": [],
                        "properties": {
                            "_id": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/_id"
                            },
                            "created_at": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/created_at"
                            },
                            "updated_at": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/updated_at"
                            },
                            "price": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/price"
                            },
                            "availibility": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/availibility"
                            },
                            "currency": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/currency"
                            },
                            "retailer_product_id": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/retailer_product_id"
                            },
                            "retailer_id": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/retailer_id"
                            },
                            "product_url": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/product_url"
                            }
                        }
                    }
                ]
            }
        }
    }
}
```

<!--
type: tab
title: Example
-->

```json
    {
        "_id": "uuid of a product",
        "retailer_group_id": {
            "retailer_id": "retailer_group_id"
        },
        "created_at": "timestamp",
        "updated_at": "timestamp",
        "category": {},
        "affiliate": {},
        "brand": {
            "brand_id": "uuid of the brand",
            "brand_name": "name of the brand"
        },
        "details": {
            "name": [{
                "value": "Name of the product",
                "slug": "name-of-the-product",
                "source": "retailer_id_1",
                "locale": "en"
            }],
            "description": [{
                "value": "Description of the product",
                "source": "retailer_id_1",
                "locale": "en"
            }],
            "global_identifiers": {
                "GTIN": "Global Trade Item Number",
                "ISBN": "Internation Standard Book Number",
                "JAN": "Japanese Article Number",
                "UPC": "Universal Product Code",
                "EAN": "International Article Number",
                "MPN": "Manufacturer Part number"
            }
        },
        "variations": {
            "options": {
                "option_name": ["option_value1", "option_value2"],
                "option_name_N": ["option_value_N1", "option_value_N2"]
            },
            "variation_type": {
                "variation_type_name": [{
                    "option_name": "option_value1",
                    "option_name_N": "option_value_N1",
                    "products": ["uuid of product1", "uuid of product2"]
                }]
            }
        }
    }
```

<!-- type: tab-end -->



<!-- theme: success -->
> ### products
Detailed information about a product (variation)
<!--
type: tab
title: Schema
-->

```json json_schema
{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "required": [],
    "properties": {
        "_id": {
            "$id": "#/properties/_id"
        },
        "group_id": {
            "$id": "#/properties/group_id"
        },
        "created_at": {
            "$id": "#/properties/created_at"
        },
        "updated_at": {
            "$id": "#/properties/updated_at"
        },
        "media": {
            "$id": "#/properties/media",
            "required": [],
            "properties": {
                "images": {
                    "$id": "#/properties/media/properties/images",
                    "items": {
                        "$id": "#/properties/media/properties/images/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/media/properties/images/items/anyOf/0",
                                "required": [],
                                "properties": {
                                    "source_url": {
                                        "$id": "#/properties/media/properties/images/items/anyOf/0/properties/source_url"
                                    },
                                    "source": {
                                        "$id": "#/properties/media/properties/images/items/anyOf/0/properties/source"
                                    },
                                    "key": {
                                        "$id": "#/properties/media/properties/images/items/anyOf/0/properties/key"
                                    }
                                }
                            }
                        ]
                    }
                },
                "videos": {
                    "$id": "#/properties/media/properties/videos",
                    "items": {
                        "$id": "#/properties/media/properties/videos/items"
                    }
                },
                "audios": {
                    "$id": "#/properties/media/properties/audios",
                    "items": {
                        "$id": "#/properties/media/properties/audios/items"
                    }
                }
            }
        },
        "global_identifiers": {
            "$id": "#/properties/global_identifiers",
            "required": [],
            "properties": {
                "gtin": {
                    "$id": "#/properties/global_identifiers/properties/gtin"
                },
                "isbn": {
                    "$id": "#/properties/global_identifiers/properties/isbn"
                },
                "jan": {
                    "$id": "#/properties/global_identifiers/properties/jan"
                },
                "upc": {
                    "$id": "#/properties/global_identifiers/properties/upc"
                },
                "ean": {
                    "$id": "#/properties/global_identifiers/properties/ean"
                },
                "mpn": {
                    "$id": "#/properties/global_identifiers/properties/mpn"
                }
            }
        },
        "details": {
            "$id": "#/properties/details",
            "required": [],
            "properties": {
                "name": {
                    "$id": "#/properties/details/properties/name",
                    "items": {
                        "$id": "#/properties/details/properties/name/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/details/properties/name/items/anyOf/0",
                                "required": [],
                                "properties": {
                                    "value": {
                                        "$id": "#/properties/details/properties/name/items/anyOf/0/properties/value"
                                    },
                                    "retailer_id": {
                                        "$id": "#/properties/details/properties/name/items/anyOf/0/properties/retailer_id"
                                    },
                                    "locale": {
                                        "$id": "#/properties/details/properties/name/items/anyOf/0/properties/locale"
                                    }
                                }
                            }
                        ]
                    }
                },
                "description": {
                    "$id": "#/properties/details/properties/description",
                    "items": {
                        "$id": "#/properties/details/properties/description/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/details/properties/description/items/anyOf/0",
                                "required": [],
                                "properties": {
                                    "value": {
                                        "$id": "#/properties/details/properties/description/items/anyOf/0/properties/value"
                                    },
                                    "retailer_id": {
                                        "$id": "#/properties/details/properties/description/items/anyOf/0/properties/retailer_id"
                                    },
                                    "locale": {
                                        "$id": "#/properties/details/properties/description/items/anyOf/0/properties/locale"
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        },
        "offers": {
            "$id": "#/properties/offers",
            "items": {
                "$id": "#/properties/offers/items",
                "anyOf": [
                    {
                        "$id": "#/properties/offers/items/anyOf/0",
                        "required": [],
                        "properties": {
                            "_id": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/_id"
                            },
                            "created_at": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/created_at"
                            },
                            "updated_at": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/updated_at"
                            },
                            "price": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/price"
                            },
                            "availibility": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/availibility"
                            },
                            "currency": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/currency"
                            },
                            "retailer_product_id": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/retailer_product_id"
                            },
                            "retailer_id": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/retailer_id"
                            },
                            "product_url": {
                                "$id": "#/properties/offers/items/anyOf/0/properties/product_url"
                            }
                        }
                    }
                ]
            }
        },
        "offers_history": {
            "$id": "#/properties/offers_history",
            "required": [],
            "properties": {
                "_id": {
                    "$id": "#/properties/offers_history/properties/_id"
                },
                "product_id": {
                    "$id": "#/properties/offers_history/properties/product_id"
                },
                "retailer_offer_history": {
                    "$id": "#/properties/offers_history/properties/retailer_offer_history",
                    "required": [],
                    "properties": {
                        "retailer_id": {
                            "$id": "#/properties/offers_history/properties/retailer_offer_history/properties/retailer_id"
                        },
                        "offers": {
                            "$id": "#/properties/offers_history/properties/retailer_offer_history/properties/offers",
                            "items": {
                                "$id": "#/properties/offers_history/properties/retailer_offer_history/properties/offers/items",
                                "anyOf": [
                                    {
                                        "$id": "#/properties/offers_history/properties/retailer_offer_history/properties/offers/items/anyOf/0",
                                        "required": [],
                                        "properties": {
                                            "_id": {
                                                "$id": "#/properties/offers_history/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/_id"
                                            },
                                            "price": {
                                                "$id": "#/properties/offers_history/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/price"
                                            },
                                            "availibility": {
                                                "$id": "#/properties/offers_history/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/availibility"
                                            },
                                            "currency": {
                                                "$id": "#/properties/offers_history/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/currency"
                                            },
                                            "created_at": {
                                                "$id": "#/properties/offers_history/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/created_at"
                                            },
                                            "product_url": {
                                                "$id": "#/properties/offers_history/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/product_url"
                                            }
                                        }
                                    }
                                ]
                            }
                        }
                    }
                }
            }
        }
    }
}
```

<!--
type: tab
title: Example
-->

```json
{
    "_id": "e0031a44-2eed-11eb-ad0d-bac9ebe90d29",
    "group_id": "dfffdd16-2eed-11eb-ad0d-bac9ebe90d29",
    "created_at": "2020-11-25T07:14:36.535881",
    "updated_at": "2020-11-25T07:14:36.535883",
    "media": {
        "images": [{
            "source_url": "https://i.ebayimg.com/images/g/XnYAAOSwcdRfFemd/s-l1600.jpg",
            "source": "48ea5b0e-18a4-11eb-b08b-e29abe947d88",
            "key": "e0031a44-2eed-11eb-ad0d-bac9ebe90d29.jpeg"
        }],
        "videos": [],
        "audios": []
    },
    "global_identifiers": {
        "gtin": "722950328842",
        "isbn": "722950328842",
        "jan": "",
        "upc": "",
        "ean": "",
        "mpn": ""
    },
    "details": {
        "name": [{
            "value": "FitKicks Women's Non-Slip Sole Active Footwear",
            "retailer_id": "48ea5b0e-18a4-11eb-b08b-e29abe947d88",
            "locale": "US"
        }],
        "description": [{
            "value": "Perfect for any busy lifestyle, you can wear to the gym, on errands, walking the dog, gardening, on the beach, or on the trail. Anywhere you go, FitKicks are with you every step of the way. FitKicks include a protective toe guard to prevent excess wear points and an elastic strap for grip and fashion.",
            "retailer_id": "48ea5b0e-18a4-11eb-b08b-e29abe947d88",
            "locale": "US"
        }]
    },
    "offers": [{
        "_id": "e0031bb6-2eed-11eb-ad0d-bac9ebe90d29",
        "created_at": "2020-11-25T07:14:36.535910",
        "updated_at": "2020-11-25T07:14:36.535912",
        "price": "19.99",
        "availibility": "inStock",
        "currency": "USD",
        "retailer_product_id": "v1|142270211121|442644532367",
        "retailer_id": "48ea5b0e-18a4-11eb-b08b-e29abe947d88",
        "product_url": "https://www.ebay.com/itm/FitKicks-Womens-Non-Slip-Sole-Active-Footwear/142270211121"
    }]
}
```

<!-- type: tab-end -->



<!-- theme: success -->
> ### offers
Historic price & availability data 
<!--
type: tab
title: Schema
-->

```json json_schema
{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "required": [],
    "properties": {
        "_id": {
            "$id": "#/properties/_id"
        },
        "product_id": {
            "$id": "#/properties/product_id"
        },
        "retailer_offer_history": {
            "$id": "#/properties/retailer_offer_history",
            "required": [],
            "properties": {
                "retailer_id": {
                    "$id": "#/properties/retailer_offer_history/properties/retailer_id"
                },
                "offers": {
                    "$id": "#/properties/retailer_offer_history/properties/offers",
                    "items": {
                        "$id": "#/properties/retailer_offer_history/properties/offers/items",
                        "anyOf": [
                            {
                                "$id": "#/properties/retailer_offer_history/properties/offers/items/anyOf/0",
                                "required": [],
                                "properties": {
                                    "_id": {
                                        "$id": "#/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/_id"
                                    },
                                    "price": {
                                        "$id": "#/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/price"
                                    },
                                    "availibility": {
                                        "$id": "#/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/availibility"
                                    },
                                    "currency": {
                                        "$id": "#/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/currency"
                                    },
                                    "created_at": {
                                        "$id": "#/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/created_at"
                                    },
                                    "product_url": {
                                        "$id": "#/properties/retailer_offer_history/properties/offers/items/anyOf/0/properties/product_url"
                                    }
                                }
                            }
                        ]
                    }
                }
            }
        }
    }
}
```

<!--
type: tab
title: Example
-->

```json
{
    "_id": "e0028868-2eed-11eb-ad0d-bac9ebe90d29",
    "product_id": "e0014dd6-2eed-11eb-ad0d-bac9ebe90d29",
    "retailer_offer_history": {
        "retailer_id": "48ea5b0e-18a4-11eb-b08b-e29abe947d88",
        "offers": [{
            "_id": "e00150b0-2eed-11eb-ad0d-bac9ebe90d29",
            "price": "19.99",
            "availibility": "outOfStock",
            "currency": "USD",
            "created_at": "2020-11-25T07:14:36.524163",
            "product_url": "https://www.ebay.com/itm/FitKicks-Womens-Non-Slip-Sole-Active-Footwear/142270211121"
        }]
    }
}
```

<!-- type: tab-end -->


## Architecture
![RexAPI](../assets/images/ProductAPI.png)


## Infrastructure
- AWS CloudFormation stack for creating owned infrastructure & deployment
- AWS Lambda for serverless computing
- API Gateway (integration) serving endpoints
- AWS Cognito authorization
- S3 bucket for storing product media
- S3 event triggers for storing uploaded product media keys
- SNS notifications for async image upload
- IAM permissions for granting access to other services
- SSM Parameter Store for sharing dynamic environment variables
- CloudWatch for logging
- AWS X-Ray for tracing
- MongoDB database

## Roadmap
- Scheduled product data update (standalone service)
- Product variations mapping
- Enriched data model
- Product Matching
- ....

## Research 
- Product identifiers
- Data aggregation / crawling
- Product Matching
- Best event driven ways to trigger Retailer Finder saving of products from retailers about which we do not include data

### Test flows for requesting the new product data by end users:

#### 1. PoC: End-to-End flow: Step Functions
Proof of Concept for requesting the unknown product by the user using parallel Step Function workflow. Proven not effective - Step Function orchestration seem like a better fit for linear postprocessing.


#### 2. PoC: End-to-End flow: SNS/SQS/EventBridge/S3 triggers
Since the workflow orchestration with the Step Function is more suitable for post processing instead of data retrieval, next End-to-End PoC workflow will be fully event driven focused. Diagram is subject to change.

https://cawemo.com/share/61527fc8-f0b0-431a-bb9b-7999cd014b23


#### 3. PoC: End-to-End flow: Steams
TBA 