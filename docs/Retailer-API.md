# Retailer-API
## Overview
Retailer API service supports Product API by aggregating and storing the data about retailers. 
<!-- theme: success -->
> Key feature of the Retailer API is to provide CRUD operations over Retailer Data Model.

## Architecture
![RexAPI](../assets/images/RetailerAPI.png)


## Infrastructure
- AWS CloudFormation stack for creating owned infrastructure & deployment
- AWS Lambda for serverless computing
- API Gateway (integration) serving endpoints
- AWS Cognito authorization
- S3 bucket for storing retailer logo & favicon
- IAM permissions for granting access to other services
- SSM Parameter Store for sharing dynamic environment variables
- CloudWatch for logging
- AWS X-Ray for tracing
- MongoDB database


## Integrations
Retailer data can be collected automatically by Retailer Finder, manually or by 3rd party integration with Retailer API's POST endpoint. 

## Roadmap
- Update & delete retailer data endpoints
- Trigger to save retailer media key to DB upon successful upload to S3
- Better error catching
- Better data validation
- Separation from Product API data storage to dedicated database.
- Data model enrichment (descriptive)
- Business logic data enrichment (affiliate commissions,..)
- Product offer data enrichment (coupons & discounts)

## Retailer data model
<!--
type: tab
title: Schema
-->

```json json_schema
{
    "$schema": "http://json-schema.org/draft-07/schema",
    "$id": "http://example.com/example.json",
    "type": "object",
    "required": [
        "_id",
        "_url",
        "retailer"
    ],
    "properties": {
        "_id": {
            "$id": "#/properties/_id",
            "type": "string"
        },
        "_url": {
            "$id": "#/properties/_url",
            "type": "string"
        },
        "retailer": {
            "$id": "#/properties/retailer",
            "type": "object",
            "required": [
                "name",
                "description",
                "logo_url",
                "logo_key",
                "locale",
                "url",
                "favicon_url",
                "favicon_key"
            ],
            "properties": {
                "name": {
                    "$id": "#/properties/retailer/properties/name",
                    "type": "string"
                },
                "description": {
                    "$id": "#/properties/retailer/properties/description",
                    "type": "string"
                },
                "logo_url": {
                    "$id": "#/properties/retailer/properties/logo_url",
                    "type": "string"
                },
                "logo_key": {
                    "$id": "#/properties/retailer/properties/logo_key",
                    "type": "string"
                },
                "locale": {
                    "$id": "#/properties/retailer/properties/locale",
                    "type": "string"
                },
                "url": {
                    "$id": "#/properties/retailer/properties/url",
                    "type": "string"
                },
                "favicon_url": {
                    "$id": "#/properties/retailer/properties/favicon_url",
                    "type": "string"
                },
                "favicon_key": {
                    "$id": "#/properties/retailer/properties/favicon_key",
                    "type": "string"
                }
            },
            "additionalProperties": false
        }
    },
    "additionalProperties": false
}
}
```

<!--
type: tab
title: Example
-->

```json
{
    "_id": "af560e98-191b-11eb-a37b-5edd22f1e481",
    "_url": "https://www.johnlewis.com",
    "retailer": {
        "name": "John Lewis & Partners | Homeware, Fashion, Electricals & More",
        "description": "Shop new season trends in homeware, furniture and fashion at John Lewis & Partners. Discover the latest beauty products and browse must-have electricals, including iPads and TVs. Find gifts and much more at johnlewis.com",
        "logo_url": "https://johnlewis.scene7.com/is/image/JohnLewis/ogFeatureImage-johnLewisPartners-040918",
        "logo_key": "af560e98-191b-11eb-a37b-5edd22f1e481_logo.png",
        "locale": "en",
        "url": "https://www.johnlewis.com",
        "favicon_url": "https://www.google.com/s2/favicons?sz=32&domain_url=https://www.johnlewis.com",
        "favicon_key": "af560e98-191b-11eb-a37b-5edd22f1e481_favicon.png"
    }
}
```

<!-- type: tab-end -->

## Research 
- DynamoDB vs MongoDB

