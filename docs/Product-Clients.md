# Product-Clients

## Overview
Product client is a service made of Retailer API clients for getting direct feed from the Retailer. 

## List of API clients:
- Amazon (client includes full support is given access only to UK market)
- eBay
- Etsy

## Infrastructure
- AWS CloudFormation stack for creating owned infrastructure & deployment
- AWS Lambda for serverless computing
- API Gateway (integration) serving endpoints
- IAM permissions for granting access to other services
- SSM Parameter Store for sharing dynamic environment variables
- CloudWatch for logging
- AWS X-Ray for tracing

## Roadmap
- Integrate as many as possible API clients
- Constantly enrich the output to support the latest product data models

